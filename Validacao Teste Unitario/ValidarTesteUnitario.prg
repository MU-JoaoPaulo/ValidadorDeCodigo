DEFINE CLASS ValidarTesteUnitario AS ProcessadorCodigoFoxPro

	PROTECTED _nHandleSql
	_nHandleSql = 0
	PROTECTED _lAtualizandoCadastro
	_lAtualizandoCadastro  = .F.
	cCodigoArquivo = ''
	cDiretorioCompletoArquivo = ''
	cNomeArquivo = ''
	cDiretorioCompativelBancoDeDados = ''
	cSupostoDiretorioCompletoTesteUnitario = ''
	cListaFuncoesPublicas = ''
	cNomeArquivoTesteUnitario = ''
	lClasseTemFuncaoPublicaNova = .F.
	
	FUNCTION INIT(tcCodigoArquivo as String, tcDiretorioCompleto as String, tnHandleConexaoSql as Number)
		DODEFAULT()
		THIS._nHandleSQL = tnHandleConexaoSql
		THIS._AlimentarPropriedadesClasse(tcCodigoArquivo, tcDiretorioCompleto )
		THIS._BuscarTodosOsMetodosDaClasse()
	ENDFUNC
	
	PROTECTED FUNCTION _BuscarTodosOsMetodosDaClasse()

		THIS._waitWindow( 'Buscando m�todos do arquivo. Aguarde...', ALLTRIM(this.cNomeArquivo), JUSTPATH(this.cDiretorioCompativelBancoDeDados))
	
		PRIVATE pcPasta
		pcPasta = THIS.cDiretorioCompativelBancoDeDados
	
		SQLEXEC( THIS._nHandleSql,;
			"SELECT "+;
			"	PASTA, METODO, TESTEUNITARIO "+;
			"FROM "+;
			"	TAMANHOMETODO "+;
			"WHERE "+;
			"	PASTA = ?pcPasta "+;
			"	AND METODO NOT LIKE '%.%'",;
			"curMetodosClasse")	
			
	ENDFUNC
	
	FUNCTION Validar(lcFuncoesSemTesteUnitario_Ret as String)
		
		LOCAL lnRetorno
		STORE 0 TO lnRetorno
	
		IF THIS._AutorizarValidacaoTesteUnitario()

			DO CASE
				CASE !FILE( THIS.cSupostoDiretorioCompletoTesteUnitario )
					
					IF FILE(STRTRAN(UPPER(THIS.cSupostoDiretorioCompletoTesteUnitario),'\TESTUNIT\','\TESTEUNIT\'))
						lnRetorno = 3 && tem com nome errado
					ELSE
						lnRetorno = 1 && n�o tem
					ENDIF
				
				OTHERWISE 				
					lcFuncoesSemTesteUnitario_Ret = THIS._RetornarFuncoesPublicasNovasSemTesteUnitario()
					IF EMPTY(lcFuncoesSemTesteUnitario_Ret)
						lnRetorno = 0
					ELSE
						lnRetorno = 2
					ENDIF
					
			ENDCASE
						
		ELSE		
			lnRetorno = 0 && Nada a se fazer			
		ENDIF
	
		RETURN lnRetorno
	
	ENDFUNC && Validar

	FUNCTION AtualizarCadastroTesteUnitario() 			 
	
		THIS._lAtualizandoCadastro = .T.
	
		LOCAL lcFuncoesPublicas, lcFuncoesPublicasComTesteUnitario, lcFuncoesPublicasSemTesteUnitario
		STORE '' TO lcFuncoesPublicas
	
		IF THIS._AutorizarValidacaoTesteUnitario() AND THIS._RetornarClasseCadastradaNoBanco()
		
			DO CASE
				CASE !FILE(THIS.cSupostoDiretorioCompletoTesteUnitario)
				
					lcFuncoesPublicas = THIS._RetornarFuncoesPublicas()
					THIS._Persistir('',lcFuncoesPublicas)
				
				OTHERWISE
				
					THIS._RastrearCoberturaDeTesteUnitarioDoArquivo(@lcFuncoesPublicasComTesteUnitario, @lcFuncoesPublicasSemTesteUnitario)
					THIS._Persistir(lcFuncoesPublicasComTesteUnitario,lcFuncoesPublicasSemTesteUnitario)
				
			ENDCASE
		
		ENDIF
		
		THIS._lAtualizandoCadastro = .F.
	
	ENDFUNC && AtualizarCadastroTesteUnitario
	
	PROTECTED FUNCTION _RastrearCoberturaDeTesteUnitarioDoArquivo(tcFuncoesPublicasComTesteUnitario_Ret as String, lcFuncoesPublicasSemTesteUnitario_Ret as String)

		THIS._waitWindow( 'Atualizando cadastro de testes unit�rios. Aguarde...', ALLTRIM(this.cNomeArquivo), JUSTPATH(this.cDiretorioCompativelBancoDeDados))

		LOCAL lcFuncoesDaClasse, lcFuncoesDaClasseSemTesteUnitario, lcCodigoTesteUnitario, ;
			laDelimitadoresMetodo[2,2], i, j, lnQtdProcedures, lnQtdFimProc, lnContProc, lcNomeMetodo, ;
			lcCodigoMetodo, lcRetorno, i, j
			
		STORE '' TO lcRetorno
		*--
		lcFuncoesDaClasse = THIS._RetornarFuncoesPublicas()
		lcFuncoesDaClasseSemTesteUnitario = lcFuncoesDaClasse
		*--
		
		laDelimitadoresMetodo[1,1] = 'PROCEDURE�'
		laDelimitadoresMetodo[1,2] = 'ENDPROC�'
		laDelimitadoresMetodo[2,1] = 'FUNCTION�'
		laDelimitadoresMetodo[2,2] = 'ENDFUNC�'
		
		lcCodigoTesteUnitario = FILETOSTR( THIS.cSupostoDiretorioCompletoTesteUnitario )		
		lcCodigoTesteUnitario = THIS.AjustarCodigo(lcCodigoTesteUnitario, THIS.cNomeArquivoTesteUnitario)
				
		FOR i=1 TO ALEN(laDelimitadoresMetodo, 1)

			*-- Ir� buscar por todos as procedures primeiro e depois ir� buscar todas as functions.
			lnQtdProcedures = OCCURS(laDelimitadoresMetodo[i,1], UPPER(lcCodigoTesteUnitario))
			
			FOR lnContProc = 1 TO lnQtdProcedures
			
				lcCodigoMetodo = STREXTRACT(lcCodigoTesteUnitario, '�' +laDelimitadoresMetodo[i,1], laDelimitadoresMetodo[i,2],  lnContProc, 3)

				IF !EMPTY(lcCodigoMetodo)

					lcCodigoMetodo= STRTRAN(lcCodigoMetodo, "AdicionarP_rocedure", "AdicionarProcedure", -1, -1, 1)					
					*-- Localiar novamente o nome do m�todo na classe para podermos recuperar o escopo do m�todo,
					*-- que ficou fora do c�digo extra�dos, pois fica antes dos delimitadores de m�todo.
					lcNomeMetodo = ALLTRIM(GETWORDNUM(MLINE(lcCodigoMetodo, 1), 1, '�'))
					lcNomeMetodo = STRTRAN(lcNomeMetodo, '�', '' )
					lcNomeMetodo = STRTRAN(lcNomeMetodo, '(', '' )
					lcNomeMetodo = STRTRAN(lcNomeMetodo, ')', '' )

					IF LEFT(UPPER(ALLTRIM(lcNomeMetodo)),4) == 'TEST'
					
						FOR j=1 TO GETWORDCOUNT(lcFuncoesDaClasse,'|')
						
							IF UPPER(ALLTRIM(GETWORDNUM(lcFuncoesDaClasse,j,'|'))) $ UPPER(ALLTRIM(lcCodigoMetodo))						
								lcFuncoesDaClasseSemTesteUnitario = STRTRAN(lcFuncoesDaClasseSemTesteUnitario,'|'+GETWORDNUM(lcFuncoesDaClasse,j,'|')+'|','|')
							ENDIF							
						ENDFOR	
											
					ENDIF

				ENDIF && !EMPTY(lcCodigoMetodo)
									
			ENDFOR && lnContProc = 1 TO lnQtdProcedures				
		ENDFOR && i=1 TO ALEN(laDelimitadoresMetodo, 1)
		
		WAIT CLEAR
		
		tcFuncoesPublicasComTesteUnitario_Ret = THIS._RetornarFuncoesPublicasComTesteUnitario(lcFuncoesDaClasse, lcFuncoesDaClasseSemTesteUnitario)
		lcFuncoesPublicasSemTesteUnitario_Ret = lcFuncoesDaClasseSemTesteUnitario

	ENDFUNC && _RastrearCoberturaDeTesteUnitarioDoArquivo
	
	PROTECTED FUNCTION _RetornarFuncoesPublicasComTesteUnitario(tcFuncoesDaClasse as String, tcFuncoesDaClasseSemTesteUnitario as String)		

		LOCAL lcRetorno, i, lcFuncao
		STORE '|' TO lcRetorno
		STORE '' TO lcFuncao
		tcFuncoesDaClasseSemTesteUnitario = UPPER(ALLTRIM(tcFuncoesDaClasseSemTesteUnitario))
		
		FOR i=1 TO GETWORDCOUNT(tcFuncoesDaClasse,'|')
		
			lcFuncao = UPPER(ALLTRIM(GETWORDNUM(tcFuncoesDaClasse,i,'|')))
		
			IF NOT (lcFuncao $ tcFuncoesDaClasseSemTesteUnitario)
			
				lcRetorno = lcRetorno + lcFuncao + '|'
							
			ENDIF
		
		ENDFOR

		RETURN lcRetorno

	ENDFUNC
	
	PROTECTED FUNCTION _AlimentarPropriedadesClasse(tcCodigoArquivo as String , tcDiretorioCompleto as String)		

		THIS.cCodigoArquivo = tcCodigoArquivo
		THIS.cDiretorioCompletoArquivo = tcDiretorioCompleto
		THIS.cNomeArquivo = ALLTRIM(JUSTFNAME(tcDiretorioCompleto))
		THIS.cNomeArquivoTesteUnitario = STRTRAN(THIS.cNomeArquivo,'.','_UNIT.')
		THIS.cDiretorioCompativelBancoDeDados = UPPER(ALLTRIM(THIS._AjustaNomeArquivoValidado(THIS.cDiretorioCompletoArquivo)))
		THIS.cSupostoDiretorioCompletoTesteUnitario = UPPER(ALLTRIM(JUSTPATH(tcDiretorioCompleto) + '\TESTUNIT\' + THIS.cNomeArquivoTesteUnitario))
		THIS.cSupostoDiretorioCompletoTesteUnitario = STRTRAN(UPPER(THIS.cSupostoDiretorioCompletoTesteUnitario),'.VC2','.PRG')
		THIS.cSupostoDiretorioCompletoTesteUnitario = STRTRAN(UPPER(THIS.cSupostoDiretorioCompletoTesteUnitario),'.SC2','.PRG')
		THIS.lClasseTemFuncaoPublicaNova = .F.

	ENDFUNC && _AlimentarPropriedadesClasse	
	
	PROTECTED FUNCTION _RetornarFuncoesPublicasNovasSemTesteUnitario()
		
		THIS._waitWindow( 'Buscando m�todos sem teste unit�rio. Aguarde...', ALLTRIM(this.cNomeArquivo), JUSTPATH(this.cDiretorioCompativelBancoDeDados))

		LOCAL lcFuncoesNovasDaClasse, lcFuncoesNovasDaClasseSemTesteUnitario, lcCodigoTesteUnitario, ;
			laDelimitadoresMetodo[2,2], i, j, lnQtdProcedures, lnQtdFimProc, lnContProc, lcNomeMetodo, ;
			lcCodigoMetodo, lcRetorno, i, j
			
		STORE '' TO lcRetorno
		*--
		lcFuncoesNovasDaClasse = THIS._RetornarFuncoesNovasPublicas()
		lcFuncoesNovasDaClasseSemTesteUnitario = lcFuncoesNovasDaClasse
		*--
		
		laDelimitadoresMetodo[1,1] = 'PROCEDURE�'
		laDelimitadoresMetodo[1,2] = 'ENDPROC�'
		laDelimitadoresMetodo[2,1] = 'FUNCTION�'
		laDelimitadoresMetodo[2,2] = 'ENDFUNC�'
		
		lcCodigoTesteUnitario = FILETOSTR( THIS.cSupostoDiretorioCompletoTesteUnitario )		
		lcCodigoTesteUnitario = THIS.AjustarCodigo(lcCodigoTesteUnitario, THIS.cNomeArquivoTesteUnitario)
				
		FOR i=1 TO ALEN(laDelimitadoresMetodo, 1)

			*-- Ir� buscar por todos as procedures primeiro e depois ir� buscar todas as functions.
			lnQtdProcedures = OCCURS(laDelimitadoresMetodo[i,1], UPPER(lcCodigoTesteUnitario))
			
			FOR lnContProc = 1 TO lnQtdProcedures
			
				lcCodigoMetodo = STREXTRACT(lcCodigoTesteUnitario, '�' +laDelimitadoresMetodo[i,1], laDelimitadoresMetodo[i,2],  lnContProc, 3)

				IF !EMPTY(lcCodigoMetodo)

					lcCodigoMetodo= STRTRAN(lcCodigoMetodo, "AdicionarP_rocedure", "AdicionarProcedure", -1, -1, 1)					
					*-- Localiar novamente o nome do m�todo na classe para podermos recuperar o escopo do m�todo,
					*-- que ficou fora do c�digo extra�dos, pois fica antes dos delimitadores de m�todo.
					lcNomeMetodo = ALLTRIM(GETWORDNUM(MLINE(lcCodigoMetodo, 1), 1, '�'))
					lcNomeMetodo = STRTRAN(lcNomeMetodo, '�', '' )
					lcNomeMetodo = STRTRAN(lcNomeMetodo, '(', '' )
					lcNomeMetodo = STRTRAN(lcNomeMetodo, ')', '' )

					IF LEFT(UPPER(ALLTRIM(lcNomeMetodo)),4) == 'TEST'
					
						FOR j=1 TO GETWORDCOUNT(lcFuncoesNovasDaClasse,'|')
						
							IF UPPER(ALLTRIM(GETWORDNUM(lcFuncoesNovasDaClasse,j,'|'))) $ UPPER(ALLTRIM(lcCodigoMetodo))						
								lcFuncoesNovasDaClasseSemTesteUnitario = STRTRAN(lcFuncoesNovasDaClasseSemTesteUnitario,'|'+GETWORDNUM(lcFuncoesNovasDaClasse,j,'|')+'|','|')
							ENDIF							
						ENDFOR	
											
					ENDIF

				ENDIF && !EMPTY(lcCodigoMetodo)
									
			ENDFOR && lnContProc = 1 TO lnQtdProcedures				
		ENDFOR && i=1 TO ALEN(laDelimitadoresMetodo, 1)
		
		WAIT CLEAR
		
		RETURN lcFuncoesNovasDaClasseSemTesteUnitario
		
	ENDFUNC && _RetornarFuncoesPublicasNovasSemTesteUnitario

	PROTECTED FUNCTION _AutorizarValidacaoTesteUnitario()				 
		
		THIS._waitWindow( 'Autorizando valida��o de teste unit�rio. Aguarde...', ALLTRIM(this.cNomeArquivo), JUSTPATH(this.cDiretorioCompativelBancoDeDados))
		
		LOCAL llRetorno
		llRetorno = .F.
		
		DO CASE 
			CASE NOT (RIGHT(UPPER(ALLTRIM(THIS.cNomeArquivo)),4) == '.PRG') && S� validaremos *.prg
				llRetorno = .F.
				
			CASE RIGHT(UPPER(ALLTRIM(THIS.cNomeArquivo)),9) == '_UNIT.PRG' && N�o validar teste unit�rio de teste unit�rio
				llRetorno = .F.
				
			CASE UPPER(ALLTRIM(LEFT(THIS.cDiretorioCompativelBancoDeDados,22))) == 'FENIX\MDL\PERSISTENCIA' ;
				AND UPPER(ALLTRIM(RIGHT(THIS.cDiretorioCompativelBancoDeDados,4))) == '.PRG' && N�o validar classes de persist�ncia
				llRetorno = .F.
							
			CASE THIS._lAtualizandoCadastro 
				llRetorno = .T.
			
			CASE THIS._ValidarArquivoCritico() AND THIS._RetornarClasseTemFuncaoPublicaNova()
				llRetorno = .T.
				
			CASE THIS._ArquivoEhUmAgente() AND THIS._RetornarClasseTemFuncaoPublicaNova()
				llRetorno = .T.
				
			CASE !THIS._RetornarClasseCadastradaNoBanco() AND THIS._RetornarClasseTemFuncoesPublicas()
				llRetorno = .T.
			
			OTHERWISE
				llRetorno = .F.
				
		ENDCASE
		
		WAIT CLEAR
		
		RETURN llRetorno
			
	ENDFUNC && _AutorizarValidacaoTesteUnitario
	
	PROTECTED FUNCTION _RetornarClasseCadastradaNoBanco()
		RETURN RECCOUNT('curMetodosClasse') > 0
	ENDFUNC
	
	PROTECTED FUNCTION _RetornarClasseTemFuncaoPublicaNova				
		RETURN THIS._BuscarFuncoesPublicasNaClasse(.T.,.F.,'')
	ENDFUNC && _RetornarClasseTemFuncaoPublicaNova

	PROTECTED FUNCTION _RetornarClasseTemFuncoesPublicas
		RETURN THIS._BuscarFuncoesPublicasNaClasse(.F.,.F.,'')
	ENDFUNC && _RetornarClasseTemFuncoesPublicas
	
	PROTECTED FUNCTION _RetornarFuncoesNovasPublicas(tcFuncoesNovasPublicas_Ret)
		
		LOCAL lcRetorno
		STORE '|' TO lcRetorno
		THIS._BuscarFuncoesPublicasNaClasse(.T.,.T.,@lcRetorno)		
		RETURN lcRetorno
	
	ENDFUNC	&& _RetornarFuncoesNovasPublicas
	
	PROTECTED FUNCTION _RetornarFuncoesPublicas()

		LOCAL lcRetorno
		STORE '|' TO lcRetorno
		THIS._BuscarFuncoesPublicasNaClasse(.F.,.T.,@lcRetorno)		
		RETURN lcRetorno
		
	ENDFUNC && _RetornarFuncoesPublicas
	
	PROTECTED FUNCTION _BuscarFuncoesPublicasNaClasse(tlConsiderarApenasFuncoesNovas as Boolean, tlRetornarNomeFuncoes as Boolean, tcNomeFuncoes_Ret as String)
		
		*-- J� validou, n�o precisa validar de novo.
		*-- Se estiver atualizando o cadastro, n�o � valida��o e sim processamento.
		IF THIS.lClasseTemFuncaoPublicaNova AND !tlRetornarNomeFuncoes  AND !THIS._AtualizandoCadastro
			RETURN .T.
		ENDIF
		
		LOCAL laDelimitadoresMetodo[2,2], i, lnQtdProcedures, lnQtdFimProc, lnContProc, lcNomeMetodo, lcCodigoMetodo, i
		
		laDelimitadoresMetodo[1,1] = 'PROCEDURE�'
		laDelimitadoresMetodo[1,2] = 'ENDPROC�'
		laDelimitadoresMetodo[2,1] = 'FUNCTION�'
		laDelimitadoresMetodo[2,2] = 'ENDFUNC�'			
		*-- Foi necess�rio retirar todos os comando SET PROCEDURE do c�digo para n�o conflitar com declara��o de procedures
		lcCodigoClasse = THIS.cCodigoArquivo
		lcCodigoClasse = STRTRAN(lcCodigoClasse, 'SET�PROCEDURE', 'SET_P_ROCEDURE', -1, -1, 1)
		lcCodigoClasse = STRTRAN(lcCodigoClasse, 'lcProcedure', 'lcP_rocedure', -1, -1, 1)
		lcCodigoClasse = STRTRAN(lcCodigoClasse, "'Procedure'", "'P_rocedure'", -1, -1, 1)
		lcCodigoClasse = STRTRAN(lcCodigoClasse, "AdicionarProcedure", "AdicionarP_rocedure", -1, -1, 1)
		lcCodigoClasse = STRTRAN(lcCodigoClasse, "'�Procedure�'", "'�P_rocedure�'", -1, -1, 1)
		lcCodigoClasse = STRTRAN(lcCodigoClasse, '"�Procedure�"', '"�P_rocedure�"', -1, -1, 1)
		lcCodigoClasse = STRTRAN(lcCodigoClasse, "�RELEASE�PROCEDURE�", "�RELEASE�PROC�", -1, -1, 1)
		lcCodigoClasse = STRTRAN(lcCodigoClasse, "�THISFORMSET�", "�SET�", -1, -1, 1)
		lcCodigoClasse = STRTRAN(lcCodigoClasse, "�SET�FUNCTION�", "�SET�F_UNCTION�", -1, -1, 1)
		
		
		FOR i=1 TO ALEN(laDelimitadoresMetodo, 1)

			*-- Ir� buscar por todos as procedures primeiro e depois ir� buscar todas as functions.
			lnQtdProcedures = OCCURS(laDelimitadoresMetodo[i,1], UPPER(lcCodigoClasse))
			
			FOR lnContProc = 1 TO lnQtdProcedures
			
				lcCodigoMetodo = STREXTRACT(lcCodigoClasse, '�' +laDelimitadoresMetodo[i,1], laDelimitadoresMetodo[i,2],  lnContProc, 3)

				IF !EMPTY(lcCodigoMetodo)

					lcCodigoMetodo= STRTRAN(lcCodigoMetodo, "AdicionarP_rocedure", "AdicionarProcedure", -1, -1, 1)					
					*-- Localiar novamente o nome do m�todo na classe para podermos recuperar o escopo do m�todo,
					*-- que ficou fora do c�digo extra�dos, pois fica antes dos delimitadores de m�todo.
					lcNomeMetodo = ALLTRIM(GETWORDNUM(MLINE(lcCodigoMetodo, 1), 1, '�'))
					lcNomeMetodo = STRTRAN(lcNomeMetodo, '�', '' )
					lcNomeMetodo = STRTRAN(lcNomeMetodo, '(', '' )
					lcNomeMetodo = STRTRAN(lcNomeMetodo, ')', '' )

					IF THIS._ValidarFuncaoPassivelDeTesteUnitario(laDelimitadoresMetodo[i,1], lcNomeMetodo, lcCodigoClasse)
						
						IF !tlRetornarNomeFuncoes
							*-- Qualquer fun��o p�blica, retorna verdadeiro.
							IF !tlConsiderarApenasFuncoesNovas						
								RETURN .T.
							ELSE
								*-- Apenas fun��es p�blicas n�o cadastradas retornam verdadeiro.
								IF !THIS._RetornarMetodoCadastradoNoBanco(lcNomeMetodo)
									THIS.lClasseTemFuncaoPublicaNova = .T.
									RETURN .T.
								ENDIF
							ENDIF && !tlConsiderarApenasFuncoesNovas
							
						ELSE
							*-- N�o deixar a fun��o retornar e concatenar todos os nomes das fun��es novas nesta vari�vel.
							IF THIS._lAtualizandoCadastro OR !THIS._RetornarMetodoCadastradoNoBanco(lcNomeMetodo)
								tcNomeFuncoes_Ret = tcNomeFuncoes_Ret + lcNomeMetodo + '|'
							ENDIF																	
						ENDIF												
																	
					ENDIF && !(ATCLINE('HIDDEN�' + laDelimitadoresMetodo[i,1] + lcNomeMetodo, lcCodigoClasse) > 0)...
					
				ENDIF && !EMPTY(lcCodigoMetodo)
									
			ENDFOR && lnContProc = 1 TO lnQtdProcedures				
		ENDFOR && i=1 TO ALEN(laDelimitadoresMetodo, 1)			

		RETURN .F.
	ENDFUNC && _BuscarFuncoesPublicasNaClasse
	
	PROTECTED FUNCTION _RetornarMetodoCadastradoNoBanco(tcNomeMetodo as String)
		
		SELECT curMetodosClasse
		GO TOP IN curMetodosClasse
		LOCATE FOR UPPER(ALLTRIM(METODO)) == UPPER(ALLTRIM(tcNomeMetodo))
		IF FOUND('curMetodosClasse')
			RETURN .T.
		ELSE
			RETURN .F.
		ENDIF
	
	ENDFUNC
	
	PROTECTED FUNCTION _ArquivoEhUmAgente()
	
		LOCAL llRetorno, lcNomeArquivo
		lcNomeArquivo = UPPER(ALLTRIM(this.cNomeArquivo))
		llRetorno = .F.
		
		IF  RIGHT(lcNomeArquivo,4) == '.PRG' ;
			AND LEFT(lcNomeArquivo,2) == 'AG' ;
			AND ('AGENTEBASE' $ UPPER(THIS.cCodigoArquivo) OR 'SINGLETONAGENTE' $ UPPER(THIS.cCodigoArquivo))
			
			llRetorno = .T.
			
		ENDIF
		
		RETURN llRetorno
	
	ENDFUNC && _ArquivoEhUmAgente
	
	PROTECTED FUNCTION _ValidarArquivoCritico()

		*-- Estes arquivos foram selecionados arbitrariamente, como cr�ticos e portanto novas fun��es demandar�o teste unit�rio.

		DO CASE
			CASE THIS.cDiretorioCompativelBancoDeDados == 'CLIENTES\HOMECENTER �NICA\TRADESQL\TRADEII\GRAVNOTAHOMECENTER.PRG'
				RETURN .T.
				
			CASE THIS.cDiretorioCompativelBancoDeDados == 'TRADESQL\SISTUSUA\HOMECENTER �NICA\SISWFATURAPENDENTES.SC2'
				RETURN .F. && POR ENQUANTO N�O VAMOS VALIDAR O SISWFATURAPENDENTES
				
			CASE THIS.cDiretorioCompativelBancoDeDados == 'TRADESQL\TRADEII\FUNCOESNFELETRONICA.PRG'
				RETURN .T.
				
			CASE THIS.cDiretorioCompativelBancoDeDados == 'CLIENTES\HOMECENTER �NICA\FRENTELOJASQL\COMUM\ORCAMENTO_HOMECENTER.VC2'
				RETURN .F. && POR ENQUANTO N�O VAMOS VALIDAR O OR�AMENTO
				
			CASE THIS.cDiretorioCompativelBancoDeDados == 'TRADESQL\TRADEII\DESENVOLVIMENTO_HC_UNICA\GRAVNOTAHOMECENTER.PRG'
				RETURN .T.
				
			OTHERWISE
				RETURN .F.
				
		ENDCASE
	
	ENDFUNC && _ValidarArquivoCritico
	
	PROTECTED FUNCTION _ValidarFuncaoPassivelDeTesteUnitario(tcDeclaracaoMetodo as String, tcNomeMetodo as String, tcCodigoClasse as String)
	
		LOCAL llRetorno
		STORE .T. TO llRetorno
		tcNomeMetodo = UPPER(ALLTRIM(tcNomeMetodo))
						
		DO CASE
		
			CASE (ATCLINE('HIDDEN�' + tcDeclaracaoMetodo + tcNomeMetodo, tcCodigoClasse) > 0)
				llRetorno = .F.
				
			CASE (ATCLINE('PROTECTED�' + tcDeclaracaoMetodo + tcNomeMetodo, tcCodigoClasse) > 0)
				llRetorno = .F.
		
			CASE (RIGHT(tcNomeMetodo,7) == '_ASSIGN') OR (RIGHT(tcNomeMetodo,7) == '_ACCESS')
				llRetorno = .F.
				
			CASE LEFT(tcNomeMetodo,4) == 'INIT' AND (SUBSTR(tcNomeMetodo,5,1) == '(' OR EMPTY(SUBSTR(tcNomeMetodo,5,1)))
				llRetorno = .F.
				
			CASE LEFT(tcNomeMetodo,7) == 'DESTROY' AND (SUBSTR(tcNomeMetodo,8,1) == '(' OR EMPTY(SUBSTR(tcNomeMetodo,8,1)))
				llRetorno = .F.
				
			CASE LEFT(tcNomeMetodo,16)	== 'CODIGOFORAMETODO'
				llRetorno = .F.
			
			OTHERWISE 
				llRetorno = .T.
				
		ENDCASE	
		
		RETURN llRetorno
	
	ENDFUNC && _ValidarFuncaoPassivelDeTesteUnitario	
	
	PROTECTED FUNCTION _MarcarFuncoesComoCobertasPorTesteUnitario(tcListaDeFuncoes as String)

		THIS._AtualizarFlagTesteUnitarioTabelaTamanhoMetodo(tcListaDeFuncoes, 1)
		
	ENDFUNC && _MarcarFuncoesComoCobertasPorTesteUnitario
	
	PROTECTED FUNCTION _MarcarFuncoesComoNaoCobertasPorTesteUnitario(tcListaDeFuncoes as String)
		
		THIS._AtualizarFlagTesteUnitarioTabelaTamanhoMetodo(tcListaDeFuncoes, 0)

	ENDFUNC && _MarcarFuncoesComoNaoCobertasPorTesteUnitario
		
	PROTECTED FUNCTION _AtualizarFlagTesteUnitarioTabelaTamanhoMetodo(tcListaDeFuncoes as String, tnValor as Number) 
	
		IF !EMPTY(STRTRAN(tcListaDeFuncoes,'|',''))
	
			PRIVATE pcPasta, pnValor
			pcPasta = THIS.cDiretorioCompativelBancoDeDados
			pnValor = tnValor
		
			tcListaDeFuncoes = UPPER(ALLTRIM(tcListaDeFuncoes))
		
			IF LEFT(tcListaDeFuncoes,1) == '|'
				tcListaDeFuncoes = RIGHT(tcListaDeFuncoes,LEN(tcListaDeFuncoes)-1)
				tcListaDeFuncoes = "'" + tcListaDeFuncoes
			ENDIF
			IF RIGHT(tcListaDeFuncoes,1) == '|'
				tcListaDeFuncoes = LEFT(tcListaDeFuncoes,LEN(tcListaDeFuncoes)-1)
				tcListaDeFuncoes = tcListaDeFuncoes + "'"
			ENDIF
			
			tcListaDeFuncoes = STRTRAN(tcListaDeFuncoes,"|","','")
				
			SQLEXEC( THIS._nHandleSql,;
				"UPDATE "+;
				"	TamanhoMetodo "+;
				"SET "+;
				"	TesteUnitario = ?pnValor "+;
				"WHERE "+;
				"	PASTA = ?pcPasta "+;
				"	AND METODO IN ("+tcListaDeFuncoes+")")

			SQLEXEC( THIS._nHandleSql,;				
				"UPDATE "+;
				"	TAMANHOMETODO "+;
				"SET "+;
				"	DataCriacaoTesteUnitario = GETDATE() "+;
				"WHERE "+;
				"	testeUnitario = 1 "+;
				"	AND PASTA NOT LIKE 'FENIX\%' "+;
				"	AND DataCriacaoTesteUnitario IS NULL")
			
		ENDIF		
	
	ENDFUNC
	
	PROTECTED FUNCTION _Persistir(tcFuncoesPublicasComTesteUnitario as String, tcFuncoesPublicasSemTesteUnitario as String)	

		*-- TamanhoMetodo.TesteUnitario = 1 (presente)
		THIS._MarcarFuncoesComoCobertasPorTesteUnitario(tcFuncoesPublicasComTesteUnitario)
		*-- TamanhoMetodo.TesteUnitario = 0 (ausente)
		THIS._MarcarFuncoesComoNaoCobertasPorTesteUnitario(tcFuncoesPublicasSemTesteUnitario)

	ENDFUNC
	
	FUNCTION DESTROY()
		DODEFAULT()
		WAIT CLEAR
	ENDFUNC

ENDDEFINE