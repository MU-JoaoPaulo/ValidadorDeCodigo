- Informa��o por coluna

	- Significado (O que � e para que serve?)
	- Poss�veis valores
	- Rotina que grava/cria
	- Rotina que altera
	- Outras tabelas relacionadas (� chave estrangeira?)
	
	* Tipo - Nullable - Valor Padr�o - PK - Identity
	
- Informa��o por tabela

	- Significado (O que � e para que serve?)
	- Rotina que grava/cria
	- Rotina que altera
	- Lista clic�vel de tabelas relacionadas:
		= Qual o relacionamento entre as duas tabelas?
		= Op��o do sistema em que este relacionamento � relevante