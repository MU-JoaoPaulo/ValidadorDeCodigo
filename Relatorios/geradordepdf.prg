DEFINE CLASS GeradorDePDF AS Custom
	*-- OBS: Esta classe s� funciona da vers�o 2.2.1 em diante do PDFCreator. Foi criada pra encapsular o componente e n�o deixar dar erro de tela se der 
	*--      algum problema no componente. N�o coloquei aqui a impress�o do Report justamente por causa do tratamento de erro, sen�o o erro do report seria 
	*--      tratado como erro do componente.
	*-- Para utilizar, siga esta estes passos:
	*-- 1) Instanciar a classe PDFCreatorMU
	*-- 2) Ver se o componente est� instalado (ou se n�o tem mensagem de erro)
	*-- 3) Chamar m�todo InicializarFilaDeImpressao. Isto vai criar uma fila pra impressora PDFCreator e o componente vai ficar esperando imprimir
	*-- 4) O programa que est� chamando esta classe vai mandar imprimir o Report na impressora PDFCreator. Automaticamente ele ir� para a fila criada.
	*-- 5) Chamar m�todo GerarPdfDaFilaDeImpressao. Isto vai gerar o PDF do report enviado pra impress�o.
	
	cMensagemErro = ''

	PROTECTED _oPDFObj
	_oPDFObj = NULL
	PROTECTED _oPDFQueue
	_oPDFQueue = NULL
	PROTECTED _lComponenteInstalado
	_lComponenteInstalado = .F.
	
	FUNCTION Init
		*-- Ver se est� em execu��o. S� pode 1 inst�ncia na m�quina.
		THIS._oPDFObj = CREATEOBJECT("PDFCreator.PDFCreatorObj","pdfcreator")
		IF NOT ISNULL(THIS._oPDFObj) AND (TYPE('THIS._oPDFObj') == 'O')
			THIS._lComponenteInstalado = .T.
		ELSE
			THIS.cMensagemErro = 'N�o foi poss�vel criar inst�ncia do componente PDFCreator. ' + CHR(13) + 'Verifique se a instala��o est� correta.'
		ENDIF
		DODEFAULT()
	ENDFUNC
	
	FUNCTION Destroy
		IF THIS._lComponenteInstalado
			IF NOT ISNULL(THIS._oPDFQueue) AND (TYPE('THIS._oPDFQueue') == 'O')
				IF THIS.PDFCreatorEmExecucao()
					THIS._oPDFQueue.ReleaseCom()
				ENDIF
				THIS._oPDFQueue = NULL
			ENDIF
			
			IF NOT ISNULL(THIS._oPDFObj) AND (TYPE('THIS._oPDFObj') == 'O')
				THIS._oPDFObj = NULL
			ENDIF
		ENDIF
		DODEFAULT()
	ENDFUNC
	
	FUNCTION ObterComponenteInstalado AS Boolean
		RETURN THIS._lComponenteInstalado
	ENDFUNC
	
	FUNCTION PDFCreatorEmExecucao AS Boolean
		IF THIS._lComponenteInstalado
			RETURN THIS._oPDFObj.IsInstanceRunning
		ELSE
			RETURN .F.
		ENDIF
	ENDFUNC
	
	FUNCTION ERROR(tnError AS INTEGER, tcMethod AS STRING, tnLine AS INTEGER)
		*-- Ignorar erro pois esta classe n�o vai gerar erro de tela se der problema com o PDFCreator
		THIS.cMensagemErro = 'Erro: '+ALLTRIM(STR(tnError)) + ' - ' + MESSAGE( ) + ' - ' + THIS.CLASS + ' - Linha: '+ ALLTRIM(STR(tnLine ))
		RETURN .T.
	ENDFUNC && ERROR
	
	FUNCTION InicializarFilaDeImpressao AS Boolean
		THIS.cMensagemErro = ''
		*-- Ver se j� tem uma inst�ncia rodando.
		IF ISNULL(THIS._oPDFObj) OR NOT (TYPE('THIS._oPDFObj') == 'O')
			THIS.cMensagemErro = 'Componente PDFCreator n�o foi criado na inicializa��o da classe. ' + CHR(13) + 'Verifique se a instala��o est� correta.'
		ENDIF
		IF EMPTY(THIS.cMensagemErro) AND TYPE('THIS._oPDFQueue') == 'O'
			IF THIS.PDFCreatorEmExecucao()
				THIS._oPDFQueue.ReleaseCom()
			ENDIF
			THIS._oPDFQueue = NULL
		ENDIF
		IF EMPTY(THIS.cMensagemErro) AND THIS.PDFCreatorEmExecucao()
			THIS.cMensagemErro = 'J� existe uma inst�ncia do PDFCreator aberta em sua m�quina. Feche-a e tente novamente.'
		ENDIF
		IF !EMPTY(THIS.cMensagemErro)
			RETURN .F.
		ENDIF
		
		*-- Criar a fila de impress�o
		THIS._oPDFQueue = CREATEOBJECT("PDFCreator.JobQueue","pdfcreator")
		IF ISNULL(THIS._oPDFQueue) OR NOT (TYPE('THIS._oPDFQueue') == 'O')
			THIS.cMensagemErro = 'N�o foi poss�vel criar inst�ncia do componente PDFCreator. ' + CHR(13) + 'Verifique se a instala��o est� correta.'
			RETURN .F.
		ENDIF
		THIS._oPDFQueue.Initialize()
		
		RETURN EMPTY(THIS.cMensagemErro)
	ENDFUNC && InicializarFilaDeImpressao
	
	FUNCTION GerarPdfDaFilaDeImpressao(tcNomeCompletoGerar AS String) AS Boolean 
		IF ISNULL(THIS._oPDFQueue) OR NOT (TYPE('THIS._oPDFQueue') == 'O') OR !THIS.PDFCreatorEmExecucao()
			THIS.cMensagemErro = '� necess�rio chamar m�todo InicializarFilaDeImpressao antes de gerar PDF.'
			RETURN .F.
		ENDIF
		
		RETURN THIS._GerarPdfDaFilaDeImpressao(tcNomeCompletoGerar)
	ENDFUNC && GerarPdfDaFilaDeImpressao
	
	FUNCTION ImprimirPdfDaFilaDeImpressao(tcNomeCompletoGerar AS String, tcImpressoraFisica AS String) AS Boolean
		*-- Objetivo: Gerar um PDF e mandar pra impressora automaticamente.
		IF ISNULL(THIS._oPDFQueue) OR NOT (TYPE('THIS._oPDFQueue') == 'O') OR !THIS.PDFCreatorEmExecucao()
			THIS.cMensagemErro = '� necess�rio chamar m�todo InicializarFilaDeImpressao antes de imprimir PDF.'
			RETURN .F.
		ENDIF
		
		RETURN THIS._GerarPdfDaFilaDeImpressao(tcNomeCompletoGerar, .T., tcImpressoraFisica)
	ENDFUNC && ImprimirPdfDaFilaDeImpressao
	
	PROTECTED FUNCTION _GerarPdfDaFilaDeImpressao(tcNomeCompletoGerar AS String, tlImprimirDireto AS Boolean, tcImpressoraFisica AS String) AS Boolean
		IF ISNULL(THIS._oPDFQueue) OR NOT (TYPE('THIS._oPDFQueue') == 'O') OR !THIS.PDFCreatorEmExecucao()
			THIS.cMensagemErro = '� necess�rio chamar m�todo InicializarFilaDeImpressao antes de gerar PDF.'
			RETURN .F.
		ENDIF
		IF !TYPE('tlImprimirDireto') = 'L'
			tlImprimirDireto = .F.
		ENDIF
		LOCAL llRetorno, loJob
		llRetorno = .F.
		*-- Esperar at� 10 segundos pra que o Job de impress�o caia a na fila
		IF THIS._oPDFQueue.WaitForJob(10)
			*-- Capturar o Job de impress�o
			loJob = THIS._oPDFQueue.NextJob()
			*-- Usar perfil padr�o
			loJob.SetProfileByGuid("DefaultGuid") && ("HighQualityGuid")
			*-- Mostrar progresso da gera��o (pra gente ver que usou o PDFCreator) 
			loJob.SetProfileSetting("ShowProgress", IIF(tlImprimirDireto,"False","True"))
			*-- N�o abrir o PDF depois que terminar
			loJob.SetProfileSetting("OpenViewer", "False")
			
			*-- Se foi passado o nome de uma impressora f�sica, ent�o al�m de gerar PDF o PDFCreator vai mandar ele pra impressora automaticamente.
			IF tlImprimirDireto
				loJob.SetProfileSetting("Printing.Enabled", "True")
				IF TYPE('tcImpressoraFisica') = 'C' AND !EMPTY(tcImpressoraFisica)
					loJob.SetProfileSetting("Printing.SelectPrinter","SelectedPrinter")
					loJob.SetProfileSetting("Printing.PrinterName",tcImpressoraFisica)
				ELSE
					loJob.SetProfileSetting("Printing.SelectPrinter","DefaultPrinter")
				ENDIF
			ENDIF
			
			*-- Converter de forma S�ncrona. S� pra informa��o, existe um m�todo assincrono tamb�m (ConvertToAsync), mas n�o vamos utiliz�-lo.
			loJob.ConvertTo(tcNomeCompletoGerar)			
			IF loJob.IsFinished AND loJob.IsSuccessful
				llRetorno = .T.  && terminou e gerou OK
			ENDIF
			
			loJob = NULL
			RELEASE loJob
		ELSE
			THIS.cMensagemErro = 'Nenhum documento foi encontrado na fila de impress�o do PDFCreator.'
		ENDIF

		THIS._oPDFQueue.ReleaseCom()
		THIS._oPDFQueue = NULL
		
		*-- Por garantia, conferir se o arquivo est� l�.
		IF llRetorno AND !FILE(tcNomeCompletoGerar)
			llRetorno = .F.
		ENDIF

		IF !llRetorno AND EMPTY(THIS.cMensagemErro)
			THIS.cMensagemErro = 'N�o foi gerado o arquivo "'+ tcNomeCompletoGerar + '".'
		ENDIF
		
		RETURN llRetorno
	ENDFUNC && _GerarPdfDaFilaDeImpressao
	
ENDDEFINE && PDFCreatorMU