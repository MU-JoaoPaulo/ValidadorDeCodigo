DEFINE CLASS EvolucaoInconsistencias as Custom

	lExportou = .F.
	PROTECTED _nHandleSql
	_nHandleSql = 0
	
	FUNCTION INIT()		 
		DODEFAULT()
		THIS._nHandleSql = oAgValidador.Conectar()
		THIS._BuscarInconsistenciasPorDesenvolvedor()
		THIS._BuscarEvolucaoDeLinhasDeCodigoSistema()
		THIS._BuscarEvolucaoDeCoberturaPorTesteUnitario()
	ENDFUNC
	
	FUNCTION GerarRelatorio

		SET PROCEDURE TO ExportadorDeRelatorio ADDITIVE

		LOCAL loExportadorDeRelatorio, lcArquivoPDF
		STORE '' TO lcArquivoPDF
		PRIVATE pdDataEmissao
		pdDataEmissao = oAgValidador.ObterData()
		
		SELECT curEvolucaoInconsistenciasProgramadores
		GO TOP IN curEvolucaoInconsistenciasProgramadores

		*!* SET STEP ON 
		*!*	IF .F.
		*!*		REPORT FORM EvolucaoInconsistencias PREVIEW
		*!*	ENDIF

		loExportadorDeRelatorio = NEWOBJECT('ExportadorDeRelatorio','ExportadorDeRelatorio.prg')
		IF loExportadorDeRelatorio.ConfiguradoParaUsarPDFCreator()
			*-- Ver se o PDFCreator est� em uso. Se estiver, avisar pra fechar.
			DO WHILE .T.
				IF loExportadorDeRelatorio.PDFCreatorEmExecucao()
					IF !MESSAGEBOX("O programa PDFCreator est� em execu��o, voc� precisa fech�-lo manualmente e clicar em SIM para continuar." +; 
							   CHR(13) + CHR(13) + "Clique em N�O se n�o quiser utiliz�-lo para gerar o PDF.",'Programa em Execu��o',4)
						EXIT
					ENDIF
				ELSE
					EXIT
				ENDIF
			ENDDO
		ENDIF
		
		lcArquivoPDF = 'C:\ValidadordeCodigo\Relatorios\Relatorio Mensal Validador De Codigo ['+STRTRAN(DTOC(DATE())+']','/','')+'.PDF'
		
		IF NOT loExportadorDeRelatorio.ExportarReportFormParaPDF('EvolucaoInconsistencias',lcArquivoPDF)
			
			IF !EMPTY(loExportadorDeRelatorio.cMensagemErro)
				MESSAGEBOX(loExportadorDeRelatorio.cMensagemErro)
			ELSE
				MESSAGEBOX("N�o foi poss�vel gerar documento [" + JUSTFNAME(lcArquivoPDF) + "] no formato Adobe PDF.")
			ENDIF
		ENDIF
		
		THIS.lExportou = EMPTY(loExportadorDeRelatorio.cMensagemErro)
		loExportadorDeRelatorio = NULL
		RELEASE loExportadorDeRelatorio				
		
		THIS._EnviarEmail('joaopaulo@microuniverso.com.br', lcArquivoPDF)

	ENDFUNC

	PROTECTED FUNCTION _BuscarInconsistenciasPorDesenvolvedor()

		SQLEXEC( THIS._nHandleSql,;
			"SELECT "+;
			"	Desenvolvedor, SUM(Total) Total, AVG(DegradacaoMedia) DegradacaoMedia, AVG(PotencialErroMedio) PotencialErroMedio, "+;
			"	AcrescimoEmFuncaoExtensa, AcrescimoDeParametro, AcrescimoDeReturn, VariaveisPublicasCriadas "+;
			"FROM "+;
			"	( "+;
			"		SELECT "+;
			"			I1.Desenvolvedor, 1 as Total, ISNULL(CAST(I1.NivelDegradacao AS int),0) as DegradacaoMedia, ISNULL(CAST(I1.PotencialDeErro AS int),0) as PotencialErroMedio, "+;
			"			(SELECT COUNT(1) FROM Inconsistencias I2 WHERE I1.Desenvolvedor = I2.Desenvolvedor AND Texto like 'Aten��o! M�todo muito extenso%') as AcrescimoEmFuncaoExtensa, "+;
			"			(SELECT COUNT(1) FROM Inconsistencias I2 WHERE I1.Desenvolvedor = I2.Desenvolvedor AND Texto like 'M�todo com muitos par�metros%') as AcrescimoDeParametro, "+;
			"			(SELECT COUNT(1) FROM Inconsistencias I2 WHERE I1.Desenvolvedor = I2.Desenvolvedor AND Texto like 'M�todo com muitos "+'"Return"%'+"') as AcrescimoDeReturn, "+;
			"			(SELECT COUNT(1) FROM Inconsistencias I2 WHERE I1.Desenvolvedor = I2.Desenvolvedor AND Texto like 'Proibido criar variavel P�BLICA%') as VariaveisPublicasCriadas "+;
			"		FROM "+;
			"			Inconsistencias I1 "+;
			"		WHERE "+;
			"			I1.PotencialDeErro != '?' AND I1.NivelDegradacao != '?' "+;
			"	) AS EvolucaoInconsistencias "+;
			"GROUP BY "+;
			"	Desenvolvedor, AcrescimoEmFuncaoExtensa, AcrescimoDeParametro, AcrescimoDeReturn, VariaveisPublicasCriadas ","curEvolucaoInconsistenciasProgramadores")

	ENDFUNC && _BuscarInconsistenciasPorDesenvolvedor
	
	PROTECTED FUNCTION _BuscarEvolucaoDeLinhasDeCodigoSistema()
				
		SQLEXEC( THIS._nHandleSql,;
			"select  "+;
			"	(select count(tm1.metodo) from tamanhometodo tm1 where tm1.DataCriacao >= '2017-08-02T12:00:00' and tm1.pasta like '%.prg') as MetodosNovosEmPRGS_MarcoZero, "+;
			"	(select count(tm2.metodo) from tamanhometodo tm2 where tm2.DataCriacao >= GETDATE() - 30 and tm2.pasta like '%.prg') as MetodosNovosEmPRGS_30Dias, "+;
			"	(select count(tm3.metodo) from tamanhometodo tm3 where tm3.DataCriacao >= GETDATE() - 60 and tm3.pasta like '%.prg') as MetodosNovosEmPRGS_60Dias, "+;
			"	(select count(tm4.metodo) from tamanhometodo tm4 where tm4.DataCriacao >= GETDATE() - 90 and tm4.pasta like '%.prg') as MetodosNovosEmPRGS_90Dias, "+;
			"	(select sum(tm5.tamanhoinicial) from tamanhometodo tm5 where tm5.DataCriacao >= '2017-08-02T12:00:00' and tm5.pasta like '%.prg') as LinhasNovasEmPRGS_MarcoZero, "+;
			"	(select sum(tm6.tamanhoinicial) from tamanhometodo tm6 where tm6.DataCriacao >= GETDATE() - 30 and tm6.pasta like '%.prg') as LinhasNovasEmPRGS_30Dias, "+;
			"	(select sum(tm7.tamanhoinicial) from tamanhometodo tm7 where tm7.DataCriacao >= GETDATE() - 60 and tm7.pasta like '%.prg') as LinhasNovasEmPRGS_60Dias, "+;
			"	(select sum(tm8.tamanhoinicial) from tamanhometodo tm8 where tm8.DataCriacao >= GETDATE() - 90 and tm8.pasta like '%.prg') as LinhasNovasEmPRGS_90Dias, "+;
			"	((select sum(tm9.tamanhoinicial) from tamanhometodo tm9 where tm9.DataCriacao >= '2017-08-02T12:00:00' and tm9.pasta like '%.prg') "+;
			"	/(select count(tm10.metodo) from tamanhometodo tm10 where tm10.DataCriacao >= '2017-08-02T12:00:00' and tm10.pasta like '%.prg')) as TamanhoMedioMetodosNovosEmPRGS_MarcoZero, "+;
			"	((select sum(tm11.tamanhoinicial) from tamanhometodo tm11 where tm11.DataCriacao >= GETDATE() - 30 and tm11.pasta like '%.prg') "+;
			"	/(select count(tm12.metodo) from tamanhometodo tm12 where tm12.DataCriacao >= GETDATE() - 30 and tm12.pasta like '%.prg')) as TamanhoMedioMetodosNovosEmPRGS_30Dias, "+;
			"	((select sum(tm13.tamanhoinicial) from tamanhometodo tm13 where tm13.DataCriacao >= GETDATE() - 60 and tm13.pasta like '%.prg') "+;
			"	/(select count(tm14.metodo) from tamanhometodo tm14 where tm14.DataCriacao >= GETDATE() - 60 and tm14.pasta like '%.prg')) as TamanhoMedioMetodosNovosEmPRGS_60Dias, "+;
			"	((select sum(tm15.tamanhoinicial) from tamanhometodo tm15 where tm15.DataCriacao >= GETDATE() - 90 and tm15.pasta like '%.prg') "+;
			"	/(select count(tm16.metodo) from tamanhometodo tm16 where tm16.DataCriacao >= GETDATE() - 90 and tm16.pasta like '%.prg')) as TamanhoMedioMetodosNovosEmPRGS_90Dias, "+;
			"	(select count(tm17.metodo) from tamanhometodo tm17 where tm17.DataCriacao >= '2017-08-02T12:00:00' and tm17.pasta like '%.sc2') as MetodosNovosEmFORMS_MarcoZero,  "+;
			"	(select count(tm18.metodo) from tamanhometodo tm18 where tm18.DataCriacao >= GETDATE() - 30 and tm18.pasta like '%.sc2') as MetodosNovosEmFORMS_30Dias,  "+;
			"	(select count(tm19.metodo) from tamanhometodo tm19 where tm19.DataCriacao >= GETDATE() - 60 and tm19.pasta like '%.sc2') as MetodosNovosEmFORMS_60Dias,  "+;
			"	(select count(tm20.metodo) from tamanhometodo tm20 where tm20.DataCriacao >= GETDATE() - 90 and tm20.pasta like '%.sc2') as MetodosNovosEmFORMS_90Dias, "+;
			"	(select sum(tm21.tamanhoinicial) from tamanhometodo tm21 where tm21.DataCriacao >= '2017-08-02T12:00:00' and tm21.pasta like '%.sc2') as LinhasNovasEmFORMS_MarcoZero, "+;
			"	(select sum(tm22.tamanhoinicial) from tamanhometodo tm22 where tm22.DataCriacao >= GETDATE() - 30 and tm22.pasta like '%.sc2') as LinhasNovasEmFORMS_30Dias, "+;
			"	(select sum(tm23.tamanhoinicial) from tamanhometodo tm23 where tm23.DataCriacao >= GETDATE() - 60 and tm23.pasta like '%.sc2') as LinhasNovasEmFORMS_60Dias, "+;
			"	(select sum(tm24.tamanhoinicial) from tamanhometodo tm24 where tm24.DataCriacao >= GETDATE() - 90 and tm24.pasta like '%.sc2') as LinhasNovasEmFORMS_90Dias, "+;
			"	((select sum(tm25.tamanhoinicial) from tamanhometodo tm25 where tm25.DataCriacao >= '2017-08-02T12:00:00' and tm25.pasta like '%.sc2') "+;
			"	/(select count(tm26.metodo) from tamanhometodo tm26 where tm26.DataCriacao >= '2017-08-02T12:00:00' and tm26.pasta like '%.sc2'))  as TamanhoMedioMetodosNovosEmFORMS_MarcoZero, "+;
			"	((select sum(tm27.tamanhoinicial) from tamanhometodo tm27 where tm27.DataCriacao >= GETDATE() - 30 and tm27.pasta like '%.sc2') "+;
			"	/(select count(tm28.metodo) from tamanhometodo tm28 where tm28.DataCriacao >= GETDATE() - 30 and tm28.pasta like '%.sc2'))  as TamanhoMedioMetodosNovosEmFORMS_30Dias, "+;
			"	((select sum(tm29.tamanhoinicial) from tamanhometodo tm29 where tm29.DataCriacao >= GETDATE() - 60 and tm29.pasta like '%.sc2') "+;
			"	/(select count(tm30.metodo) from tamanhometodo tm30 where tm30.DataCriacao >= GETDATE() - 60 and tm30.pasta like '%.sc2'))  as TamanhoMedioMetodosNovosEmFORMS_60Dias, "+;
			"	((select sum(tm31.tamanhoinicial) from tamanhometodo tm31 where tm31.DataCriacao >= GETDATE() - 90 and tm31.pasta like '%.sc2') "+;
		 	"	/(select count(tm32.metodo) from tamanhometodo tm32 where tm32.DataCriacao >= GETDATE() - 90 and tm32.pasta like '%.sc2'))  as TamanhoMedioMetodosNovosEmFORMS_90Dias, "+;
			"	(select count(tm33.metodo) from tamanhometodo tm33 where tm33.DataCriacao >= '2017-08-02T12:00:00' and tm33.pasta like '%.vc2') as MetodosNovosEmCLASSES_MarcoZero, "+;
			"	(select count(tm34.metodo) from tamanhometodo tm34 where tm34.DataCriacao >= GETDATE() - 30 and tm34.pasta like '%.vc2') as MetodosNovosEmCLASSES_30Dias, "+;
			"	(select count(tm35.metodo) from tamanhometodo tm35 where tm35.DataCriacao >= GETDATE() - 60 and tm35.pasta like '%.vc2') as MetodosNovosEmCLASSES_60Dias, "+;
			"	(select count(tm36.metodo) from tamanhometodo tm36 where tm36.DataCriacao >= GETDATE() - 90 and tm36.pasta like '%.vc2') as MetodosNovosEmCLASSES_90Dias, "+;
			"	(select sum(tm37.tamanhoinicial) from tamanhometodo tm37 where tm37.DataCriacao >= '2017-08-02T12:00:00' and tm37.pasta like '%.vc2') as LinhasNovasEmCLASSES_MarcoZero, "+;
			"	(select sum(tm38.tamanhoinicial) from tamanhometodo tm38 where tm38.DataCriacao >= GETDATE() - 30 and tm38.pasta like '%.vc2') as LinhasNovasEmCLASSES_30Dias, "+;
			"	(select sum(tm39.tamanhoinicial) from tamanhometodo tm39 where tm39.DataCriacao >= GETDATE() - 60 and tm39.pasta like '%.vc2') as LinhasNovasEmCLASSES_60Dias, "+;
			"	(select sum(tm40.tamanhoinicial) from tamanhometodo tm40 where tm40.DataCriacao >= GETDATE() - 90 and tm40.pasta like '%.vc2') as LinhasNovasEmCLASSES_90Dias, "+;
			"	((select sum(tm41.tamanhoinicial) from tamanhometodo tm41 where tm41.DataCriacao >= '2017-08-02T12:00:00' and tm41.pasta like '%.vc2') "+;
			"	/(select count(tm42.metodo) from tamanhometodo tm42 where tm42.DataCriacao >= '2017-08-02T12:00:00' and tm42.pasta like '%.vc2'))  as TamanhoMedioMetodosNovosEmCLASSES_MarcoZero, "+;
			"	((select sum(tm43.tamanhoinicial) from tamanhometodo tm43 where tm43.DataCriacao >= GETDATE() - 30 and tm43.pasta like '%.vc2') "+;
			"	/(select count(tm44.metodo) from tamanhometodo tm44 where tm44.DataCriacao >= GETDATE() - 30 and tm44.pasta like '%.vc2'))  as TamanhoMedioMetodosNovosEmCLASSES_30Dias, "+;
			"	((select sum(tm45.tamanhoinicial) from tamanhometodo tm45 where tm45.DataCriacao >= GETDATE() - 60 and tm45.pasta like '%.vc2') "+;
			"	/(select count(tm46.metodo) from tamanhometodo tm46 where tm46.DataCriacao >= GETDATE() - 60 and tm46.pasta like '%.vc2'))  as TamanhoMedioMetodosNovosEmCLASSES_60Dias, "+;
			"	((select sum(tm47.tamanhoinicial) from tamanhometodo tm47 where tm47.DataCriacao >= GETDATE() - 90 and tm47.pasta like '%.vc2') "+;
			"	/(select count(tm48.metodo) from tamanhometodo tm48 where tm48.DataCriacao >= GETDATE() - 90 and tm48.pasta like '%.vc2'))  as TamanhoMedioMetodosNovosEmCLASSES_90Dias ","curEvolucaoLinhasDeCodigoSistema")			
			
	ENDFUNC	&& _BuscarEvolucaoDeLinhasDeCodigoSistema
	
	PROTECTED FUNCTION _BuscarEvolucaoDeCoberturaPorTesteUnitario()
		 
		THIS._BuscarCoberturaTotal()
		THIS._BuscarCoberturaCodigoCompartilhado()
		this._BuscarCoberturaCodigoHCU()
		
	ENDFUNC && _BuscarEvolucaoDeCoberturaPorTesteUnitario
	
	PROTECTED FUNCTION _BuscarCoberturaCodigoHCU()
	
		SQLEXEC( THIS._nHandleSql,;	
			"SELECT "+;
			"	ISNULL((SELECT  "+;
			"		SUM(TESTEUNITARIO) "+;
			"	FROM  "+;
			"		TAMANHOMETODO  "+;
			"	WHERE "+;
			"		(PASTA LIKE '%\HOMECENTER �NICA\%' OR PASTA LIKE '%Desenvolvimento_HC_Unica\%') "+;
			"		AND (PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"		AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"		AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"		AND LEFT(METODO,1) != '_' "+;
			"		AND TESTEUNITARIO = 1),0) as FuncoesCobertas, "+;
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO  "+;
			"		WHERE "+;
			"			(PASTA LIKE '%\HOMECENTER �NICA\%' OR PASTA LIKE '%Desenvolvimento_HC_Unica\%') "+;
			"			AND (PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 0),0) as FuncoesDescobertas, "+;
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO  "+;
			"		WHERE "+;
			"			(PASTA LIKE '%\HOMECENTER �NICA\%' OR PASTA LIKE '%Desenvolvimento_HC_Unica\%') "+;
			"			AND (PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 1 "+;
			"			AND (DataCriacaoTesteUnitario + 30) >= GETDATE()),0) as Ultimos30Dias, "+;
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO  "+;
			"		WHERE "+;
			"			(PASTA LIKE '%\HOMECENTER �NICA\%' OR PASTA LIKE '%Desenvolvimento_HC_Unica\%') "+;
			"			AND (PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 1 "+;
			"			AND (DataCriacaoTesteUnitario + 60) >= GETDATE()),0) as Ultimos60Dias, "+;
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO  "+;
			"		WHERE "+;
			"			(PASTA LIKE '%\HOMECENTER �NICA\%' OR PASTA LIKE '%Desenvolvimento_HC_Unica\%') "+;
			"			AND (PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 1 "+;
			"			AND (DataCriacaoTesteUnitario + 90) >= GETDATE()),0) as Ultimos90Dias ",;
			"curCoberturaCodigoHCU")
			
	ENDFUNC && _BuscarCoberturaCodigoHCU	
	
	PROTECTED FUNCTION _BuscarCoberturaCodigoCompartilhado()
	
		SQLEXEC( THIS._nHandleSql,;	
			"SELECT "+;
			"	ISNULL((SELECT  "+;
			"		SUM(TESTEUNITARIO) "+;
			"	FROM  "+;
			"		TAMANHOMETODO  "+;
			"	WHERE "+;
			"		(PASTA LIKE 'TRADESQL\%' OR PASTA LIKE 'FRENTELOJASQL\%') "+;
			"		AND (PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"		AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"		AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"		AND LEFT(METODO,1) != '_' "+;
			"		AND TESTEUNITARIO = 1),0) as FuncoesCobertas, "+;
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO  "+;
			"		WHERE "+;
			"			(PASTA LIKE 'TRADESQL\%' OR PASTA LIKE 'FRENTELOJASQL\%') "+;
			"			AND (PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 0),0) as FuncoesDescobertas, "+;
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO  "+;
			"		WHERE "+;
			"			(PASTA LIKE 'TRADESQL\%' OR PASTA LIKE 'FRENTELOJASQL\%') "+;
			"			AND (PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 1 "+;
			"			AND (DataCriacaoTesteUnitario + 30) >= GETDATE()),0) as Ultimos30Dias, "+;
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO  "+;
			"		WHERE "+;
			"			(PASTA LIKE 'TRADESQL\%' OR PASTA LIKE 'FRENTELOJASQL\%') "+;
			"			AND (PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 1 "+;
			"			AND (DataCriacaoTesteUnitario + 60) >= GETDATE()),0) as Ultimos60Dias, "+;
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO  "+;
			"		WHERE "+;
			"			(PASTA LIKE 'TRADESQL\%' OR PASTA LIKE 'FRENTELOJASQL\%') "+;
			"			AND (PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 1 "+;
			"			AND (DataCriacaoTesteUnitario + 90) >= GETDATE()),0) as Ultimos90Dias ",;
			"curCoberturaCodigoCompartilhado")
			
	ENDFUNC && _BuscarCoberturaCodigoCompartilhado
	
	PROTECTED FUNCTION _BuscarCoberturaTotal()
	
		SQLEXEC( THIS._nHandleSql,;	
			"SELECT "+;
			"	ISNULL((SELECT "+;
			"		SUM(TESTEUNITARIO) "+;
			"	FROM "+;
			"		TAMANHOMETODO "+;
			"	WHERE "+;
			"		(PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"		AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"		AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"		AND LEFT(METODO,1) != '_' "+;
			"		AND TESTEUNITARIO = 1),0) as FuncoesCobertas, "+;
			"	ISNULL((SELECT "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM "+;
			"			TAMANHOMETODO "+;
			"		WHERE "+;
			"			(PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 0),0) as FuncoesDescobertas, "+;			
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO  "+;
			"		WHERE "+;
			"			(PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 1 "+;
			"			AND (DataCriacaoTesteUnitario + 30) >= GETDATE()),0) as Ultimos30Dias, "+;
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO  "+;
			"		WHERE "+;
			"			(PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 1 "+;
			"			AND (DataCriacaoTesteUnitario + 60) >= GETDATE()),0) as Ultimos60Dias, "+;
			"	ISNULL((SELECT  "+;
			"			COUNT(TESTEUNITARIO) "+;
			"		FROM  "+;
			"			TAMANHOMETODO "+;
			"		WHERE "+;
			"			(PASTA LIKE '%.PRG' AND PASTA NOT LIKE 'FENIX\%' AND PASTA NOT LIKE '_UNIT.PRG') "+;
			"			AND METODO NOT IN ('CODIGOFORAMETODO','INIT','DESTROY') "+;
			"			AND (METODO NOT LIKE '%_ASSIGN' AND METODO NOT LIKE '%_ACCESS') "+;
			"			AND LEFT(METODO,1) != '_' "+;
			"			AND TESTEUNITARIO = 1 "+;
			"			AND (DataCriacaoTesteUnitario + 90) >= GETDATE()),0) as Ultimos90Dias",;
			"curCoberturaTotal")
							
	ENDFUNC && _BuscarCoberturaTotal
	
	PROTECTED FUNCTION _EnviarEmail(tcDestinatario as String, tcArquivoPDF as String)

		oAgValidador.EnviarEmail(; 
			'aspmx.l.google.com',; && Ferramenta
			tcDestinatario,; && Destinat�rios
			'Relat�rio Mensal - Validador de C�digo',; && Assunto
			'Ol�, bom dia/tarde a todos. <BR><BR>'+;
			'� importante ressaltar que os n�meros referentes �s inconsist�ncias por desenvolvedor, s�o um indicador mas n�o refletem indisciplina em todos os casos. <BR>'+;
			'Por vezes � preciso arcar com os custos de acumular inconsist�ncias para realizar alguma refatora��o, em outros casos inconsist�ncias s�o inevit�veis. <BR><BR>'+;
			'Tendo isto em mente, segue o relat�rio anexo. <BR><BR>'+;
			'Att, Jo�o Paulo Martins Castanheira',; && Texto
			tcArquivoPDF) && Anexo

	ENDFUNC
	
	FUNCTION DESTROY()
		
		IF USED('curEvolucaoInconsistenciasProgramadores')
			USE IN curEvolucaoInconsistenciasProgramadores
		ENDIF
	
		IF USED("curEvolucaoLinhasDeCodigoSistema")
			USE IN curEvolucaoLinhasDeCodigoSistema
		ENDIF
		
		IF USED("curCoberturaTotal")
			USE IN curCoberturaTotal
		ENDIF
		
		IF USED("curCoberturaCodigoCompartilhado")
			USE IN curCoberturaCodigoCompartilhado
		ENDIF
		
		IF USED("curCoberturaCodigoHCU")
			USE IN curCoberturaCodigoHCU
		ENDIF				
		
		IF THIS.lExportou
			oAgValidador.WritePrivStr('ultimorelatorioenviado',ALLTRIM(DTOC(oAgValidador.ObterData())))
		ENDIF

	ENDFUNC		
	
ENDDEFINE