DEFINE CLASS ExportadorDeRelatorio as Custom
	** <NOTA>
	** Autor: Fabiano - 30/11/2015
	** Objetivo: Classe incialmente criada para exportar report form pra PDF de uma forma centralizada. Depois pode ser extendida pra outros formatos.
	**           Ela vai ver se tem PDFCreator na m�quina e exportar usando ele, se n�o tiver vai continuar usando a MindsEyeReportEngine.
	**           O PDFCreator � bem melhor que o MindsEyeReportEngine.
	** </NOTA>
	
	**************************
	*-- Propriedades - In�cio
	**************************
	lGerarLogLinkTime = .F.   && Usada pra saber se vai gravar LogLinkTime ao gerar PDF pelo Mind's Eye Report
	nCodErro = 0
	cMensagemErro = ''
	**************************
	*-- Propriedades - Fim
	**************************
	
	FUNCTION ExportarReportFormParaPDF AS Boolean
		PARAMETERS tcNomeReportForm, tcNomeCompletoArquivo
		
		LOCAL lcNomeArquivo, lcDiretorio, lcNomeCompletoGerar, lnRegistrada, llRetorno

		llRetorno = .F.
		THIS.nCodErro = 0
		THIS.cMensagemErro = ''
		
		IF EMPTY(tcNomeReportForm)
			THIS.cMensagemErro = 'Informe o nome do report que ser� exportado.'
			RETURN .F.
		ENDIF
		IF EMPTY(tcNomeCompletoArquivo)
			THIS.cMensagemErro = 'Informe o nome do arquivo no qual o relat�rio ser� gravado.'
			RETURN .F.
		ENDIF
		*-- Separar o nome do arquivo (sem extens�o PDF) e o diret�rio		
		lcDiretorio = SUBSTR(tcNomeCompletoArquivo,1,RAT('\',tcNomeCompletoArquivo))
		lcNomeArquivo = SUBSTR(tcNomeCompletoArquivo,RAT('\',tcNomeCompletoArquivo)+1)
		IF UPPER(RIGHT(lcNomeArquivo,4)) == '.PDF'
			lcNomeArquivo = LEFT(lcNomeArquivo,LEN(lcNomeArquivo)-4)
		ENDIF
		lcNomeCompletoGerar = lcDiretorio + lcNomeArquivo + '.PDF'
		*-- Ver se arquivo j� existe e se est� aberto.
		IF THIS.ArquivoEmUso(lcNomeCompletoGerar)
			THIS.cMensagemErro = 'O arquivo "' + lcNomeArquivo + '" j� existe e est� aberto no diret�rio "' + lcDiretorio + '".' + CHR(13) + CHR(13) +;
				  				 'Exclua-o ou feche-o para que ele possa ser sobreposto.'
			RETURN .F.
		ENDIF
		*-- Deletar o arquivo para ser sobreposto.
		IF FILE(lcNomeCompletoGerar)
			DELETE FILE &lcNomeCompletoGerar
		ENDIF
		****************
		*-- Exporta��o
		****************
		IF THIS.ConfiguradoParaUsarPDFCreator()
			*-- Usar PDF Creator
			llRetorno = THIS._ExportarComPDFCreator(tcNomeReportForm,lcNomeCompletoGerar)
		ENDIF
		
		RETURN llRetorno
	ENDFUNC
	
	PROTECTED FUNCTION _ExportarComPDFCreator AS Boolean
		PARAMETERS tcNomeReportForm, tcNomeCompletoGerar
		
		LOCAL loGeradorDePdf, llRetorno, lcPrinterAtual
		llRetorno = .F.
		*-- Usar uma classe espec�fica pra tratar o PDFCreator
		loGeradorDePdf = NEWOBJECT("GeradorDePDF","GeradorDePDF.prg")
		IF !EMPTY(loGeradorDePdf.cMensagemErro)
			THIS.cMensagemErro = loGeradorDePdf.cMensagemErro
			loGeradorDePdf = NULL
			RETURN .F.
		ENDIF
		IF loGeradorDePdf.PDFCreatorEmExecucao()
			THIS.cMensagemErro = 'J� existe uma inst�ncia do PDFCreator aberta em sua m�quina. Feche-a e tente novamente.'
			loGeradorDePdf = NULL
			RETURN .F.
		ENDIF
		*lcPrinterAtual = SYS(6)
		lcPrinterAtual = SET("PRINTER", 2)
		
		*-- 1�) Iniciar a fila de impress�o do objeto PDF Creator. Isto vai deixar o componente pronto, s� esperando a impress�o.
		IF loGeradorDePdf.InicializarFilaDeImpressao()
		
			*-- 2�) Fazer a impress�o do relat�rio na impressora PDFCreator. Isto vai mandar a impress�o pra fila que o componente criou.
			SET PRINTER TO NAME 'PDFCreator'
			REPORT FORM &tcNomeReportForm NOCONSOLE TO PRINTER
			IF EMPTY(lcPrinterAtual)
				SET PRINTER TO DEFAULT
			ELSE
				SET PRINTER TO NAME (lcPrinterAtual)
			ENDIF
			
			*-- 3�) Pegar o documento da fila de impress�o e gerar o PDF
			llRetorno = loGeradorDePdf.GerarPdfDaFilaDeImpressao(tcNomeCompletoGerar)
			IF !llRetorno
				THIS.cMensagemErro = loGeradorDePdf.cMensagemErro
			ENDIF
		ELSE
			THIS.cMensagemErro = loGeradorDePdf.cMensagemErro
		ENDIF
		loGeradorDePdf = NULL
		
		IF !llRetorno AND EMPTY(THIS.cMensagemErro)
			THIS.cMensagemErro = 'N�o foi gerado o arquivo "'+ tcNomeCompletoGerar + '".'
		ENDIF
		
		RETURN llRetorno
	ENDFUNC && _ExportarComPDFCreator 
	
	PROTECTED FUNCTION _ExistePrinterPDFCreator
		*-- Objetivo: Verificar se o PDFCreator est� na lista de impressoras
		LOCAL lnQtd, lnPos
		lnQtd = APRINTERS(paPrinters)
		FOR lnPos = 1 TO lnQtd
			IF UPPER(ALLTRIM(paPrinters[lnPos,1])) == 'PDFCREATOR'
				RETURN .T.
			ENDIF
		NEXT
		RETURN .F.
	ENDFUNC && _ExistePrinterPDFCreator
	
	FUNCTION ArquivoEmUso
		PARAMETERS tcNomeCompletoArquivo
		
		LOCAL lnHandle
		*-- Ver se arquivo j� existe e se est� aberto.
		IF FILE(tcNomeCompletoArquivo) 
			lnHandle = FOPEN(tcNomeCompletoArquivo,12) && Abrir read\write.
			IF  (lnHandle = -1) && Arquivo est� aberto.
				RETURN .T.
			ENDIF
			FCLOSE(lnHandle)
		ENDIF
		
		RETURN .F.
	ENDFUNC && ArquivoEmUso
	
	FUNCTION ConfiguradoParaUsarPDFCreator AS Boolean
		IF THIS._ExistePrinterPDFCreator()
			RETURN .T.
		ENDIF
		RETURN .F.
	ENDFUNC
	
	FUNCTION PDFCreatorEmExecucao AS Boolean
		LOCAL loGeradorDePdf, llRetorno
		llRetorno = .F.
		*-- Usar uma classe espec�fica pra tratar o PDFCreator
		loGeradorDePdf = NEWOBJECT("GeradorDePDF","GeradorDePDF.prg")
		IF EMPTY(loGeradorDePdf.cMensagemErro)
			IF loGeradorDePdf.PDFCreatorEmExecucao()
				llRetorno = .T.
			ENDIF
		ENDIF
		loGeradorDePdf = NULL
		RETURN llRetorno
	ENDFUNC
		
ENDDEFINE  && ExportadorDeRelatorio