DEFINE CLASS AgArquivo as Custom
	
	FUNCTION ValidarTamanhoFuncaoArquivo(tcNomeArquivo as String, tcDescricaoInicioMetodo as String, tcDescricaoFinalMetodo as String, tnTamanhoMaximoPermitido as Integer, tnTamanho_Ret as Integer)
		LOCAL lcCodigo, lnPosInicio, lnPosFim, lnTotLinha, lnLinha, lcLinha, lnPos, lnTamanho
		lcCodigo = FILETOSTR(tcNomeArquivo)
		
		lnPosInicio = AT(tcDescricaoInicioMetodo, lcCodigo) 
		lcCodigo = SUBSTR(lcCodigo, lnPosInicio)
		lnPosFim = AT(tcDescricaoFinalMetodo, lcCodigo)
		lcCodigo = LEFT(lcCodigo, lnPosFim + (LEN(tcDescricaoFinalMetodo) - 1))
		lnTamanho = 0

		lnTotLinha = ALINES(alinhas, lcCodigo )
		
		FOR lnLinha=1 TO lnTotLinha
			lcLinha = alinhas[lnLinha]
			
			IF THIS._ValidarLinhaComentario(lcLinha)
				lnPos = ATCC('&'+'&', lcLinha )-1

				IF lnPos > 0
					lcLinha = LEFT( lcLinha , lnPos)
				ENDIF
				
				lnTamanho = lnTamanho + LEN(lcLinha)
			ENDIF
		ENDFOR
		
		IF (lnTamanho > tnTamanhoMaximoPermitido)
			tnTamanho_Ret = lnTamanho - tnTamanhoMaximoPermitido
		ELSE
			tnTamanho_Ret = 0
		ENDIF 
		
		RETURN (lnTamanho > tnTamanhoMaximoPermitido)
	ENDFUNC
	
	PROTECTED FUNCTION _ValidarLinhaComentario(tcLinha as String)
		LOCAL lnEncontrou, lcLinha
		lnEncontrou = 0
		
		lcLinha = THIS._TratarLinha(tcLinha)
		
		lnEncontrou = lnEncontrou + ATC("�*", lcLinha) + ATC("�*-", lcLinha) + ;
			ATC("� *", lcLinha) + ATC("�  *", lcLinha) + ATC("�"+"&"+"&", lcLinha)
			
		IF (LEFT(lcLinha,1) == '*') OR (LEFT(lcLinha,2) == "&"+"&")
			lnEncontrou = lnEncontrou + 1
		ENDIF 
		
		RETURN (lnEncontrou < 1)
	ENDFUNC 
	
	PROTECTED FUNCTION _TratarLinha(tcLinha As String)
		LOCAL lcRetorno, llVerifica, lnPosicao
		
		lcRetorno = ALLTRIM(tcLinha)
		lcRetorno = ALLTRIM(STRTRAN(lcRetorno, CHR(9), '�')) &&& Troca TAB por espaco
		
		lnPosicao = ATC("�" + SPACE(1), lcRetorno)
		
		IF lnPosicao > 0
			lcRetorno = LEFT(lcRetorno, lnPosicao) + ALLTRIM(SUBSTR(lcRetorno, lnPosicao + 1))
		ENDIF
		
		RETURN lcRetorno
	ENDFUNC 
ENDDEFINE