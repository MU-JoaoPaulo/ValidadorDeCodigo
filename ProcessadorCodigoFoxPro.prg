DEFINE CLASS ProcessadorCodigoFoxPro as Custom

	PROTECTED _lValidandoPasta_RepositValCodigo
	_lValidandoPasta_RepositValCodigo = .F.	
	PROTECTED _nArquivosTotaisDiretorio
	_nArquivosTotaisDiretorio = 0
	PROTECTED _nArquivosValidados
	_nArquivosValidados = 0

	FUNCTION INIT()
		LOCAL loexcecao
		#DEFINE CRLF CHR(13)+CHR(10)
		SET MEMOWIDTH TO 10000
	ENDFUNC &&Init
	
	PROTECTED FUNCTION _ValidarComentario(tcLinha as String, tcProximaLinha as String)

		tcLinha = UPPER(ALLTRIM(tcLinha))

		LOCAL llRetorno, lnPosicaoInicioComentario, lnPosicaoPontoEVirgula, llProximaLinhaEhComentario
		STORE .T. TO llRetorno

		TRY
			llProximaLinhaEhComentario = (LEFT(tcProximaLinha,1) == '*' OR LEFT(tcProximaLinha,2) == "&"+"&")

			DO CASE
				CASE NOT llProximaLinhaEhComentario AND LEFT(tcLinha,1) == '*'
					IF ';' $ tcLinha
						llRetorno = .F.
					ENDIF
					
				CASE NOT llProximaLinhaEhComentario AND ATCC('&'+'&', tcLinha) > 0
					lnPosicaoInicioComentario = ATCC('&'+'&', tcLinha)
					lnPosicaoPontoEVirgula = ATC(';', tcLinha)
					IF lnPosicaoInicioComentario < lnPosicaoPontoEVirgula
						llRetorno = .F.
					ENDIF
					
				OTHERWISE
					llRetorno = .T.
			ENDCASE

		CATCH
			llRetorno = .T.
		ENDTRY
		RETURN llRetorno
	ENDFUNC
	
	PROTECTED FUNCTION _waitWindow( tcMensagemLinha1, tcMensagemLinha2, tcMensagemLinha3 )
		
		LOCAL lcMensagem, lcPercentual
		lcMensagem = ''
		lcPercentual = ''
		
		IF NOT EMPTY(tcMensagemLinha1)
			tcMensagemLinha1 = ALLTRIM(tcMensagemLinha1)
			IF LEN(tcMensagemLinha1 ) > 100
				lcMensagem = lcMensagem + tcMensagemLinha1
			ELSE
				IF EMPTY(tcMensagemLinha2)
					lcMensagem = lcMensagem + PADR( tcMensagemLinha1, 100, ' ' )
				ELSE
					lcMensagem = lcMensagem + tcMensagemLinha1
				ENDIF 
			ENDIF 
			*--
		ENDIF 
		IF NOT EMPTY(tcMensagemLinha2)
			IF EMPTY(tcMensagemLinha3 )
				lcMensagem = lcMensagem+ CRLF+ PADR( ALLTRIM(LOWER(tcMensagemLinha2)), 100, ' ' )
			ELSE
				lcMensagem = lcMensagem+ CRLF+ LOWER(LEFT(tcMensagemLinha2,100))
			ENDIF 
		ENDIF 
		IF NOT EMPTY(tcMensagemLinha3 )
			tcMensagemLinha3=THIS._AjustaNomeArquivoValidado(tcMensagemLinha3)
			lcMensagem = lcMensagem+ CRLF+ PADR( ALLTRIM(LOWER(tcMensagemLinha3)), 100, ' ' )
		ENDIF 
		
		IF THIS._nArquivosTotaisDiretorio > 0 AND THIS._nArquivosValidados > 0 AND THIS._lValidandoPasta_RepositValCodigo
			lcPercentual = ALLTRIM(STR(ROUND((100 * (THIS._nArquivosValidados / 18830)),1)))	
		ENDIF
		
		IF NOT EMPTY(lcMensagem)
			IF !EMPTY(lcPercentual)
				WAIT WINDOW NOWAIT NOCLEAR LEFT('['+lcPercentual+'%]' + lcMensagem , 255)
			ELSE
				WAIT WINDOW NOWAIT NOCLEAR LEFT(lcMensagem , 255)
			ENDIF
			
		ENDIF 
	ENDFUNC	
	
	FUNCTION AjustarCodigo(tcCodigoClasse, tcNomePrg)
		
		LOCAL lnTotLinha, lnPos, lcLinha, lcLinhaAux, lcLinhaAnterior, llIgnorarLinha, ;
				lcCodClasse, lcCodForaDoMetodo, lcCodMetodoForaDoClasse, llAchouClasse, llAchouMetodoSemClasse,; 
				lcAux, lcInicioMetodo, lcNomeMetodo, llPrg, loExcecao, lcCodigoClasseSemDocumentacaoSemEspaco

		STORE '' TO lcLinhaAnterior, lcCodForaDoMetodo, lcCodMetodoForaDoClasse,; 
			lcAux, lcCodClasse, lcNomeMetodo, lcCodigoClasseSemDocumentacaoSemEspaco

		TRY 
		
			*--
			llPrg = UPPER(RIGHT(ALLTRIM(tcNomePrg),4)) = '.PRG'
			*--
			tcCodigoClasse  = STRTRAN( tcCodigoClasse, '_COMATTRIB(4)', '_COMATTRIB[4]' )
			tcCodigoClasse  = STRTRAN( tcCodigoClasse, 'PARAMNOME =', 'PARAMNOME=' )
			tcCodigoClasse  = STRTRAN( tcCodigoClasse, 'PARAMNOME= ', 'PARAMNOME=' )

			lnTotLinha = ALINES(alinhas, tcCodigoClasse )


			FOR lnLinha=1 TO lnTotLinha
			
				lcLinha = alinhas[lnLinha]
				lcLinha = ALLTRIM(STRTRAN(lcLinha, CHR(9), ' ')) &&& Troca TAB por espaco
				
				IF NOT (LEFT(lcLinha,1) == '*') AND NOT (LEFT(lcLinha,2) == "&"+"&")
				
					lcLinha = STRTRAN( lcLinha, '!', ' NOT ' )&&& substitui "!" por "NOT"+Espaco
					lcLinha = STRTRAN( lcLinha, '$', ' $ ' )&&& da espaco entre "$"


					lcLinha = STRTRAN( lcLinha, ' (', '(' )&&& retira espaco antes do "("
					lcLinha = STRTRAN( lcLinha, '   ', ' ' )&&& retira espaco antes do "("
					lcLinha = STRTRAN( lcLinha, '   ', ' ' )&&& retira espaco antes do "("
					lcLinha = STRTRAN( lcLinha, '  ', ' ' )&&& retira espaco antes do "("
					lcLinha = STRTRAN( lcLinha, '  ', ' ' )&&& retira espaco antes do "("
					lcLinha = STRTRAN( lcLinha, '  ', ' ' )&&& retira espaco antes do "("
					lcLinha = STRTRAN( lcLinha, ' (', '(' )&&& retira espaco antes do "("
					lcLinha = STRTRAN( lcLinha, ' (', '(' )&&& retira espaco antes do "("
					lcLinha = STRTRAN( lcLinha, ' AND(', ' AND (', -1, -1, 1 )
					lcLinha = STRTRAN( lcLinha, ' OR(', ' OR (' , -1, -1, 1 )
					lcLinha = STRTRAN( lcLinha, '==', '=' )


					lcLinha  = ALLTRIM(lcLinha ) && Retira espacos no inicio e fim
					lcLinhaUpper = UPPER(lcLinha )

					*** retira comentarios apos comando fox
					lnPos = ATCC('&'+'&', lcLinha )-1

					IF lnPos > 0
						lcLinha = LEFT( lcLinha , lnPos)
					ENDIF
					lcLinha = ALLTRIM(lcLinha)
					*--
					IF NOT EMPTY(lcLinha ) 
						lcLinha = ALLTRIM(lcLinha)
						*-- junta as linhas que terminam com ";"
						IF RIGHT( lcLinha,1) == ';'
							lcLinhaAnterior = lcLinhaAnterior + ' '+ LEFT(lcLinha, LEN(lcLinha)-1)
							lcLinha = ''
						ELSE
							IF NOT EMPTY(lcLinhaAnterior )
								lcLinha = lcLinhaAnterior  + ' '+lcLinha
								lcLinha = STRTRAN(lcLinha, '   ', ' ' )
								lcLinha = STRTRAN(lcLinha, '  ', ' ' )
								lcLinha = STRTRAN(lcLinha, '  ', ' ' )
							ENDIF
							lcLinhaAnterior = ''

							IF NOT LEFT(lcLinha,1) $ '*&' 
								*--
								lcLinhaAux= '�'+lcLinha+'�'
								lcLinhaAux= STRTRAN( lcLinhaAux, ' (','(')
								lcLinhaAux= STRTRAN( lcLinhaAux, ' [','[')
								lcLinhaAux= STRTRAN( lcLinhaAux, ' ','�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '>', '�>' )
								lcLinhaAux= STRTRAN( lcLinhaAux, '<', '�<' )
								lcLinhaAux= STRTRAN( lcLinhaAux, '=','�=�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '�=�=�','�=�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '+','�+�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '-','�-�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '*','�*�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '/','�/�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '(','(�')  &&&& nao tem "�" antes do parenteses
								lcLinhaAux= STRTRAN( lcLinhaAux, ')','�)�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '[','[�')
								lcLinhaAux= STRTRAN( lcLinhaAux, ']','�]�')
								lcLinhaAux= STRTRAN( lcLinhaAux, ',','�,�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '@', '�' )
								lcLinhaAux= STRTRAN( lcLinhaAux, '?', '?�' )
								lcLinhaAux= STRTRAN( lcLinhaAux, '"', '�"�' )
								lcLinhaAux= STRTRAN( lcLinhaAux, "'", "�'�" )
								lcLinhaAux= STRTRAN( lcLinhaAux, '���','�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '��','�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '��','�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '(�)','()')
								lcLinhaAux= STRTRAN( lcLinhaAux, '[�]','[]')
								lcLinhaAux= STRTRAN( lcLinhaAux, '�in�','�IN�', -1, -1, 1 )
								lcLinhaAux= STRTRAN( lcLinhaAux, '�IN(','�IN�(', -1, -1, 1 )
								lcLinhaAux= STRTRAN( lcLinhaAux, '�AND(','�AND�(', -1, -1, 1 )
								lcLinhaAux= STRTRAN( lcLinhaAux, '�OR(','�OR�(', -1, -1, 1 )
								lcLinhaAux= STRTRAN( lcLinhaAux, '.f.','.F.', -1, -1, 1 )
								lcLinhaAux= STRTRAN( lcLinhaAux, '.t.','.T.', -1, -1, 1 )
								lcLinhaAux= STRTRAN( lcLinhaAux, 'RETURN(�.F.�)','RETURN�.F.�', -1, -1, 1 )
								lcLinhaAux= STRTRAN( lcLinhaAux, 'RETURN(�.T.�)','RETURN�.T.�', -1, -1, 1 )
								lcLinhaAux= STRTRAN( lcLinhaAux, "�'�'�", "�''�" )
								lcLinhaAux= STRTRAN( lcLinhaAux, '�"�"�', '�""�')
								lcLinhaAux= STRTRAN( lcLinhaAux, '�-�>', '->')


								IF UPPER(lcLinhaAux) == '�IF�.F.�'
									llIgnorarLinha = .T.
								ENDIF 

								IF NOT llIgnorarLinha AND NOT( lcLinhaAux == '�' )
								
									lcAux = UPPER(LEFT( lcLinhaAux, 25 ))
									
									IF '|�ENDFUNC�' $ '|'+lcAux OR  '|�ENDPROC�' $ '|'+lcAux 
										IF llPrg 
											IF !EMPTY(lcNomeMetodo)
												IF '�FUNCTION�' $ UPPER(lcNomeMetodo) AND '|�ENDPROC�' $ '|'+lcAux 													
													lcLinhaAux = STRTRAN(lcLinhaAux, '�ENDPROC�', '�ENDFUNC�' )
												ENDIF 
												IF '�PROCEDURE�' $ UPPER(lcNomeMetodo) AND '|�ENDFUNC�' $ '|'+lcAux 
													lcLinhaAux = STRTRAN(lcLinhaAux, '�ENDFUNC�', '�ENDPROC�' )
												ENDIF
											ENDIF
										ENDIF
										lcInicioMetodo = ''
										lcNomeMetodo = ''
									ENDIF
									
									IF '|�FUNCTION�' $ '|'+lcAux OR '|�PROCEDURE�' $ '|'+lcAux OR '|�PROTECT�FUNCTION�' $ '|'+lcAux OR '|�PROTECTED�FUNCTION�' $ '|'+lcAux OR '|�PROTECTED�PROCEDURE�' $ '|'+lcAux OR '|�HIDDEN�FUNCTION�' $ '|'+lcAux OR '|�HIDDEN�PROCEDURE�' $ '|'+lcAux 
										lcNomeMetodo = lcLinhaAux
										IF NOT EMPTY(lcInicioMetodo )
											IF NOT EMPTY(lcInicioMetodo )
												IF '|�FUNCTION�' $ '|'+lcAux
													lcLinhaAux = '�ENDFUNC�'+CRLF+lcLinhaAux 
												ENDIF 
												IF '|�PROCEDURE�' $ '|'+lcAux
													lcLinhaAux = '�ENDPROC�'+CRLF+lcLinhaAux 
												ENDIF 
											ENDIF											
										ENDIF 
										lcInicioMetodo = lcNomeMetodo 
									ENDIF

									IF NOT( LEFT( lcAux, 5 ) == '�SET�' ) AND NOT( LEFT( lcAux, 10 ) == '�#INCLUDE�') 
										IF NOT llAchouClasse
											IF '�DEFINE�CLASS�' $ lcAux
												llAchouClasse = .T.
												llAchouMetodoSemClasse = .F.
											ENDIF
										ENDIF 
										IF NOT llAchouClasse AND NOT llAchouMetodoSemClasse 
											IF '�FUNCTION�' $ lcAux OR '�PROCEDURE�' $ lcAux 
												llAchouMetodoSemClasse = .T.
											ENDIF
										ENDIF 
									ENDIF && IF NOT( LEFT( lcAux, 5 ) == '�SET�' ) AND NOT( LEFT( lcAux, 10 ) == '�#INCLUDE�') 
									**--
									IF llAchouClasse 
										lcCodClasse = lcCodClasse + lcLinhaAux + CRLF	
									ELSE
										IF llAchouMetodoSemClasse
											lcCodMetodoForaDoClasse = lcCodMetodoForaDoClasse +lcLinhaAux+CRLF
										ELSE
											lcCodForaDoMetodo = lcCodForaDoMetodo +lcLinhaAux+CRLF
										ENDIF 
									ENDIF 

									IF NOT llAchouClasse AND llAchouMetodoSemClasse AND ( '�ENDFUNC�' $ lcAux OR  '�ENDPROC�' $ lcAux )
										llAchouMetodoSemClasse = .F.
									ENDIF 
									IF llAchouClasse AND '�ENDDEFINE�' $ lcAux
										llAchouClasse = .F.
									ENDIF 
								ENDIF 
								IF llIgnorarLinha  AND UPPER(lcLinhaAux) == '�ENDIF�'
									llIgnorarLinha = .F.
								ENDIF 

							ENDIF && IF NOT llLinhaDocumentacao AND NOT LEFT(lcLinha,1) $ '*&'
						ENDIF && IF RIGHT( lcLinha,1) == ';'
					ENDIF
				ENDIF &&IF NOT LEFT(lcLinha,1) $ '*&' AND NOT (LEFT(lcLinhaUpper ,2) == "&"+"&")
			ENDFOR
			
			lcCodigoClasseSemDocumentacaoSemEspaco = lcCodClasse 
			
			IF NOT EMPTY(lcCodForaDoMetodo+lcCodMetodoForaDoClasse)
				
				lcCodigoClasseSemDocumentacaoSemEspaco = lcCodigoClasseSemDocumentacaoSemEspaco +;
					'�DEFINE�CLASS�'+ALLTRIM(tcNomePrg)+'�AS�SEMHERANCA�'+CRLF
				IF NOT EMPTY(lcCodMetodoForaDoClasse)
					lcCodigoClasseSemDocumentacaoSemEspaco = lcCodigoClasseSemDocumentacaoSemEspaco +lcCodMetodoForaDoClasse
				ENDIF 
				IF NOT EMPTY(lcCodForaDoMetodo)
					lcCodigoClasseSemDocumentacaoSemEspaco = lcCodigoClasseSemDocumentacaoSemEspaco +;
					'�FUNCTION�CodigoForaMetodo�'+crlf+lcCodForaDoMetodo+'�ENDFUNC�'+crlf 
				ENDIF 
				lcCodigoClasseSemDocumentacaoSemEspaco = lcCodigoClasseSemDocumentacaoSemEspaco + '�ENDDEFINE�'+CRLF
			ENDIF 
			
			lcCodigoClasseSemDocumentacaoSemEspaco = STRTRAN(lcCodigoClasseSemDocumentacaoSemEspaco , '��', '�' )		
			lcCodigoClasseSemDocumentacaoSemEspaco = STRTRAN(lcCodigoClasseSemDocumentacaoSemEspaco, 'SET�PROCEDURE', 'SET_P_ROCEDURE', -1, -1, 1)
			lcCodigoClasseSemDocumentacaoSemEspaco = STRTRAN(lcCodigoClasseSemDocumentacaoSemEspaco, 'lcProcedure', 'lcP_rocedure', -1, -1, 1)
			lcCodigoClasseSemDocumentacaoSemEspaco = STRTRAN(lcCodigoClasseSemDocumentacaoSemEspaco, "'Procedure'", "'P_rocedure'", -1, -1, 1)
			lcCodigoClasseSemDocumentacaoSemEspaco = STRTRAN(lcCodigoClasseSemDocumentacaoSemEspaco, "AdicionarProcedure", "AdicionarP_rocedure", -1, -1, 1)
			lcCodigoClasseSemDocumentacaoSemEspaco = STRTRAN(lcCodigoClasseSemDocumentacaoSemEspaco, "'�Procedure�'", "'�P_rocedure�'", -1, -1, 1)
			lcCodigoClasseSemDocumentacaoSemEspaco = STRTRAN(lcCodigoClasseSemDocumentacaoSemEspaco, '"�Procedure�"', '"�P_rocedure�"', -1, -1, 1)
			lcCodigoClasseSemDocumentacaoSemEspaco = STRTRAN(lcCodigoClasseSemDocumentacaoSemEspaco, "�RELEASE�PROCEDURE�", "�RELEASE�PROC�", -1, -1, 1)
			lcCodigoClasseSemDocumentacaoSemEspaco = STRTRAN(lcCodigoClasseSemDocumentacaoSemEspaco, "�THISFORMSET�", "�SET�", -1, -1, 1)
			lcCodigoClasseSemDocumentacaoSemEspaco = STRTRAN(lcCodigoClasseSemDocumentacaoSemEspaco, "�SET�FUNCTION�", "�SET�F_UNCTION�", -1, -1, 1)			
	
		CATCH TO loExcecao
			SET STEP ON 
		ENDTRY
	
		RETURN lcCodigoClasseSemDocumentacaoSemEspaco
		
	ENDFUNC &&_RetirarComentarios

	PROTECTED FUNCTION _AjustaNomeArquivoValidado( tcPasta )

		LOCAL lcRet, lnPos
		lcRet = UPPER(ALLTRIM(tcPasta))
		lcRet = STRTRAN( lcRet, '\\', '\' )
	
		*-- RETIRAR OS PREFIXOS DOS REPOSIT�RIOS COMO: "C:\REPOSITORIO FOX\"
		DO CASE
		
			CASE UPPER(ALLTRIM(LEFT(lcRet,9))) == 'CLIENTES\'
			
				*-- N�O FAZER NADA, O DIRET�RIO J� EST� PRONTO.
		
			CASE ATC('\CLIENTES\', lcRet) > 0
			
				lcRet = RIGHT(lcRet,LEN(lcRet) - ATC('\CLIENTES\',lcRet))
			
			CASE ATC('\FENIX\', lcRet) > 0
			
				lcRet = RIGHT(lcRet,LEN(lcRet) - ATC('\FENIX\',lcRet))
			
			CASE ATC('\TRADESQL\', lcRet) > 0
			
				lcRet = RIGHT(lcRet,LEN(lcRet) - ATC('\TRADESQL\',lcRet))
			
			CASE ATC('\FRENTELOJASQL\', lcRet) > 0
			
				lcRet = RIGHT(lcRet,LEN(lcRet) - ATC('\FRENTELOJASQL\',lcRet))
			
			CASE ATC('\UTILITARIO\', lcRet) > 0
			
				lcRet = RIGHT(lcRet,LEN(lcRet) - ATC('\UTILITARIO\',lcRet))
			
			OTHERWISE

				IF SUBSTR(lcRet, 2,2 ) = ':\'
					lnPos = AT('\', lcRet, 2)
					IF lnPos  > 0
						lcRet = SUBSTR( lcRet , lnPos+1, LEN(lcRet))
					ENDIF 
				ENDIF 
			
		ENDCASE

		lcRet = RIGHT(lcRet,254)
		*--
		RETURN lcRet
	ENDFUNC && _AjustaNomeArquivoValidado
				
	PROTECTED FUNCTION _DefinirDelimitadorFinalDoCabecalhoDaClasse(tcCodigoClasse)

		LOCAL lcRetorno, lnPrimeiraFunction, lnPrimeiraProcedure, lnPrimeiroEndDefine
		
		STORE '' TO lcRetorno		
		lnPrimeiraFunction = ATCLINE('FUNCTION', tcCodigoClasse)
		lnPrimeiraProcedure = ATCLINE('PROCEDURE', tcCodigoClasse)
		lnPrimeiroEndDefine = ATCLINE('ENDDEFINE', tcCodigoClasse)
		
		DO CASE
			CASE lnPrimeiraProcedure == 0 AND lnPrimeiraFunction > 0 && Classe sem 'PROCEDURE'
				lcRetorno = 'FUNCTION'
				
			CASE lnPrimeiraFunction == 0 AND lnPrimeiraProcedure > 0 && Classe sem 'FUNCTION'
				lcRetorno = 'PROCEDURE'
				
			CASE lnPrimeiraFunction < lnPrimeiraProcedure && 1� 'FUNCTION' anterior ao 1� 'PROCEDURE'
				lcRetorno = 'FUNCTION'
				
			CASE lnPrimeiraProcedure < lnPrimeiraFunction && 1� 'PROCEDURE' anterior ao 1� 'FUNCTION'
				lcRetorno = 'PROCEDURE'
				
			OTHERWISE && Classe sem m�todos
				lcRetorno = 'ENDDEFINE'
						
		ENDCASE
		
		RETURN lcRetorno
	
	ENDFUNC	&& _DefinirDelimitadorFinalDoCabecalhoDaClasse

ENDDEFINE